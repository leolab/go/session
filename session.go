package session

import (
	"crypto/md5"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"gitlab.com/leolab/go/errs"
)

type Config struct {
	CookieName string
	CookieTime time.Duration
	ClearTime  time.Duration
}

type Store struct {
	cfg Config

	_store map[string]*Rec
	_lock  *sync.RWMutex
	_rnd   *rand.Rand
	_wg    *sync.WaitGroup
	_proc  chan interface{}
}

func NewStore(cfg *Config) *Store {
	m := &Store{
		cfg: *cfg,

		_store: make(map[string]*Rec),
		_proc:  make(chan interface{}),
		_rnd:   rand.New(rand.NewSource(time.Now().UnixNano())),
		_lock:  &sync.RWMutex{},
		_wg:    &sync.WaitGroup{},
	}
	return m
}

func (s *Store) Init() {}

func (s *Store) Start() {
	s._wg.Add(1)
	go s._exec()
}

func (s *Store) Stop() {
	close(s._proc)
	s._wg.Wait()
}

func (s *Store) _exec() {
	defer s._wg.Done()
	tick := *time.NewTicker(s.cfg.ClearTime)
	defer func() { tick.Stop() }()
	for {
		select {
		case <-tick.C:
			s._clear()
		case <-s._proc:
			return
		}
	}
}

func (s *Store) _clear() {
	s._lock.Lock()
	defer s._lock.Unlock()

	for k, ss := range s._store {
		if ss.Expired.Before(time.Now()) {
			delete(s._store, k)
		}
	}
}

func (s *Store) Get(w http.ResponseWriter, r *http.Request) *Rec {
	s._lock.Lock()
	defer s._lock.Unlock()

	c, e := r.Cookie(s.cfg.CookieName)
	if e != nil {
		if e == http.ErrNoCookie {
			errs.RaiseError(ErrCookieError, "No cookie, create new")
			c = s._newCookie(r)
		} else {
			errs.RaiseError(ErrCookieError, e.Error())
		}
	}
	if _, ok := s._store[c.Value]; !ok {
		s._store[c.Value] = s._newRec(r, c)
	}
	sr, ok := s._store[c.Value]
	if !ok {
		errs.RaiseError(ErrStoreError, "NO COOKIE STORE VALUE")
		return nil
	}
	if sr.Hash != s._getHash(r) {
		errs.RaiseError(ErrInvalidSessionHash, "Invalid session hash", map[string]interface{}{"Req": r})
		// :?: Удалить сессию
		// :?: Создать новую сессию
		// :?: Удалить куку
		// :?: Создать (переписать) куку
		//return nil
	}
	//Продление куки
	c.Expires = time.Now().Add(s.cfg.CookieTime)
	c.Path = "/"
	sr.Expired = c.Expires
	http.SetCookie(w, c)
	fmt.Printf("Req: %s, Cookie: %+v\n", r.URL.Path, c)
	return sr
}

func (s *Store) _newRec(r *http.Request, c *http.Cookie) *Rec {
	sr := &Rec{
		SSID:    c.Value,
		Hash:    s._getHash(r),
		Expired: c.Expires,
		Data:    make(map[string]interface{}),
	}
	return sr
}

func (s *Store) _newCookie(r *http.Request) *http.Cookie {
	c := &http.Cookie{
		Path:    "/",
		Name:    s.cfg.CookieName,
		Value:   s._newSSID(r),
		Expires: time.Now().Add(s.cfg.CookieTime),
		MaxAge:  int(s.cfg.CookieTime.Seconds()),
	}
	return c
}

func (s *Store) _newSSID(r *http.Request) string {
	b := make([]byte, 8)
	_, err := s._rnd.Read(b)
	if err != nil {
		errs.RaiseError(errs.ErrError, err.Error())
		//panic(err.Error())
		return ""
	}
	return fmt.Sprintf("%x", b)
}

func (s *Store) _getHash(r *http.Request) string {
	v0 := r.RemoteAddr
	v1 := r.Header.Get("User-Agent")
	v2 := r.Header.Get("X-Forwarded-For")
	hash := md5.Sum([]byte(v0 + v1 + v2))
	return fmt.Sprintf("%x", hash)
}
