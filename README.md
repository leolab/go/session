# Реализация HTTP-сессий

## Usage
Simple HTTP server with sessions:
```go

import (
    "gitlab.com/leolab/go/session"
    
    "gitlab.com/leolab/go/errs"
)

var Session *session.Store

func serveRequest (w http.ResponseWriter, r *http.Request){
    s:=Session.Get(w,r)
    // You code with session data
}


func main(){
    var err *errs.Err

    sc := &session.Config{
        CookieName: "SessionStoreKey", // You session cookie value
        CookieTime: time.Second * 30,  // Timelife sesssion
        ClearTime: time.Second*10,     // Time for check session timeouts and clear data
    }

    Session = session.NewStore(sc)
    Session.Start();

    http.HandleFunc("/", serveRequest)
    err := http.ListerAndServe(":8080", nil)

    Session.Stop()

}

```