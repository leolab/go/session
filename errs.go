package session

import "gitlab.com/leolab/go/errs"

const (
	ErrInvalidSessionHash errs.ErrCode = "ErrInvalidSessionHash" //Хеш сессии не совпадает с хешем запроса
	ErrStoreError         errs.ErrCode = "ErrStoreError"         //Ошибка сохранения сессии
	ErrCookieError        errs.ErrCode = "ErrCookieError"        //Ошибка куки
)
