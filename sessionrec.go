package session

import "time"

type Rec struct {
	SSID    string
	Hash    string
	Expired time.Time
	Data    map[string]interface{}
}
