package session

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/leolab/go/errs"
)

var ses *Store

var (
	cs  []*http.Cookie
	srv *httptest.Server
)

var HandleRequest http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
	s := ses.Get(w, r)
	if _, ok := s.Data["test"]; ok {
		io.WriteString(w, s.Data["test"].(string)+"\n")
	}
	s.Data["test"] = "TestData"
	io.WriteString(w, "SSID: "+s.SSID)
}

func TestInit(t *testing.T) {
	var err *errs.Err
	sc := &Config{
		CookieName: "SessionStoreTest",
		CookieTime: time.Second * 30,
		ClearTime:  time.Second * 10,
	}
	ses = NewStore(sc)
	if err != nil {
		panic(err)
	}
	if ses == nil {
		panic("Session store is nil")
	}
	ses.Start()
	srv = httptest.NewServer(HandleRequest)
	t.Log("Server started")
}

func TestReqFirst(t *testing.T) {
	rsp, e := http.Get(srv.URL + "/")
	if e != nil {
		t.Fatal(e)
	}
	defer rsp.Body.Close()
	data, e := ioutil.ReadAll(rsp.Body)
	if e != nil {
		t.Fatal(e)
	}
	t.Log(string(data))

	cs = rsp.Cookies()
	if len(cs) == 0 {
		t.Fatal("No cookies")
	}
	for _, c := range cs {
		t.Log("Cookie: ", c.Name, "=", c.Value)
	}
}

func TestReqSec(t *testing.T) {

	req, e := http.NewRequest("GET", srv.URL, nil)
	if e != nil {
		t.Fatal(e)
	}
	for _, c := range cs {
		req.AddCookie(c)
	}

	cli := &http.Client{}
	rsp, e := cli.Do(req)
	if e != nil {
		t.Fatal(e)
	}
	defer rsp.Body.Close()
	data, e := ioutil.ReadAll(rsp.Body)
	if e != nil {
		t.Fatal(e)
	}
	t.Log(string(data))

	cs = rsp.Cookies()
	if len(cs) == 0 {
		t.Fatal("No cookies")
	}
	for _, c := range cs {
		t.Log("Cookie: ", c.Name, "=", c.Value)
	}
}

func TestFinish(t *testing.T) {
	t.Log("Finishing...")
	ses.Stop()
	t.Log("Success")
}
